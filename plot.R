source("lib/utilities.R")
source("lib/graph.R")

plot_tpg <- function(tpg_file, save_as) {
  load(tpg_file)
  graph <- build_graph(trained_tpg)
  export_graph(graph, file_name = save_as)
  writeLines(paste("The plot has been saved as", save_as))
}

the_args <- getArgs()

if ("help" %in% names(the_args)) {
  cat("Usage: Rscript plot.R [--help] tpg_file= [save_as=tpg.png]\n")
  cat("\t--help: show help\n")
  cat("\ttpg_file: path to the trained TPG;\n")
  cat("\tsave_as: save the visualization of the TPG under this name (must have 'png', 'pdf', 'svg', or 'ps' format)'\n")
  quit(save="no", status=0)
}

if(!is.null(the_args[["tpg_file"]])) {
  tpg_file <- the_args[["tpg_file"]]
} else { stop("Missing tpg_file argument") }
if (!is.null(the_args[["save_as"]])) {
  save_as <- the_args[["save_as"]]
} else { save_as <- "tpg.png" }

plot_tpg(tpg_file=tpg_file, save_as=save_as)
