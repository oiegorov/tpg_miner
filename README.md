# TPG miner #

An implementation of the *tpg-miner* algorithm in R.  

*tpg-miner* allows to mine a task precedence graph (TPG) from a set of symbolic traces
and use it for anomaly detection in trace streams.
The algorithm is presented in the paper "Mining Task Precedence Graphs from
Real-Time Embedded System Traces" by Oleg Iegorov and Sebastian Fischmeister which will be published in the Proceedings of the RTAS
conference in April 2018.

## Reproducing the results ##

You can reproduce the results of Case Study I (Section VIII.C in the paper) by running the commands shown below. CAN traces provided by the [HCRL lab](http://ocslab.hksecurity.net/Datasets/CAN-intrusion-dataset) 
are in the 'car_hacking_traces' folder.

1. Train a TPG on the first 1000 lines of 'DoS Attack' and 'Fuzzy Attack' traces (found in the 'car_hacking_traces/training_data' subfolder):     
`$ Rscript train.R training_dir=car_hacking_traces/training_data`
2. Visualize the trained TPG:     
`$ Rscript plot.R tpg_file=tpg.RData`
3. Detect anomalies in the 'gear_dataset_test.csv' trace using the trained TPG:    
`$ Rscript detect.R --verbose test_trace_file=car_hacking_traces/gear_dataset_test.csv tpg_file=tpg.RData`
4. Detect anomalies in the 'RPM_dataset_test.csv' trace using the trained TPG:    
`$ Rscript detect.R --verbose test_trace_file=car_hacking_traces/RPM_dataset_test.csv tpg_file=tpg.RData`

## Usage ##

The following explains how to use the *tpg_miner* tool to train TPGs and use them for anomaly detection in symbolic traces.
The traces must be in CSV format, and the name of the column containing task names
must be known (e.g., 'id'). You can use the '--help' flag with each script to see the available parameters.

### Training ###

All the training traces must be located in a
separate folder (e.g., 'training_data'). The following command will train a TPG on a set of traces found in the 'training_data' folder and save it as tpg.RData (the column containing task names in each trace has the name 'id'):

`$ Rscript train.R training_dir=training_data target_column=id`

### Plotting ###

To plot a TPG saved in the 'tpg.RData' file, run

`$ Rscript plot.R tpg_file=tpg.RData`

The plot will be saved as "tpg.png" in the current folder.

### Anomaly detection ###

To detect anomalies in a trace 'test_trace.csv' using the TPG saved as tpg.RData, run

`$ Rscript detect.R --verbose test_trace_file=test_trace.csv
tpg_file=tpg.RData`

The script will output all the detected anomalies and provide
details about each anomaly (line number and violated occurrence
pattern). If the --verbose flag is not set, the script will only output
the total number of detected anomalies.